open List
open PMap
open Topol

exception Wrong_answer
exception To_little_numbers

let print_list l = 
  let f ak a = 
    let _ = print_int(a); in
    let _ = print_char(' '); in
    0
  in fold_left f 0 l;;

let print_pMap myPmap = 
  let f k c =
    print_int (k) ; print_string (": ") ;  
    print_int (c) ; print_newline() in
  print_newline() ; iter f myPmap ; print_newline()
  
let print_graph g =
  let f v l =
    let p a = print_char (' ') ; print_int (a) 
    in print_int (v) ; print_char (':') ; 
    (List.iter p l) ; print_endline("") in
  iter f g ; print_endline ("")

let v1 = 1, 4::[3];;
let v2 = 2, [];;
let v3 = 3, [4; 2];;
let v4 = 4, [6];;
let v5 = 5, [1];;
let v6 = 6, [];;
let u1 = 1, [3];;
let u4 = 4, [1; 6];;

let g = [v1 ; v3 ; v4 ; v5];;
let a = topol g;;
let _ = print_list a;;

(* let g = [u1 ; v3 ; u4 ; v5];; *)
let a = topol g;;
let _ = print_list a;;

let rec make_order (order, which) n m =
  if (n = 0) then (order, which)
  else let v = Random.int m in
  if (mem v which) then make_order (order, which) n m
  else let order = add n v order in
  let which = add v n which in
  make_order (order, which) (n - 1) m
  
let (order, which) = make_order (empty, empty) 10 100
let _ = print_pMap order;;
let _ = print_pMap which;;

let rec my_graph_gen order which graph n e =
  if (e = 0) then graph
  else let v = (Random.int n) + 1 in
  let w = (Random.int n) + 1 in
  if (v = w) then my_graph_gen order which graph n e else
  let (v, w) = if (v > w) then (w, v) else (v, w) in
  let a = find v order in let b = find w order in 
  let graph = add a (b :: (find a graph)) graph in
  my_graph_gen order which graph n (e - 1)
  
let rec dfs (graph, vis, prev, ok) v = 
  if (not ok) then (graph, vis, prev, ok) 
  else if (mem v vis) then (graph, vis, prev, true)
  else (* let _ = print_int(v) in let _ = print_char(' ') in*)
  if (mem v prev) then raise Wrong_answer
  else let vis = add v true vis in
  let (_, vis, _, ok) = fold_left dfs (graph, vis, prev, ok) (find v graph) in
  (graph, vis, prev, ok)    
  
let check (prev, graph, ok) v = 
  if (not ok) then (prev, graph, ok)
  else (*let _ = print_newline() in let _ = print_int(v) in let _ = print_string(": ") in*)
  let (_, _, _, ok) = dfs (graph, empty, prev, true) v in
  ((add v true prev), graph, ok)
    
let make_test m n e = 
  if (m < n) then raise To_little_numbers else
  let (order, which) = make_order (empty, empty) n m in
  let _ = print_pMap which in
  let _ = print_pMap order in
  let graph = fold (fun v acc -> add v [] acc) order empty in
  let graph = my_graph_gen order which graph n e in
  let _ = print_graph graph in
  let lgraph = foldi (fun v l acc -> (v, l) :: acc) graph [] in
  let res = topol lgraph in
  let _ = print_list res in
  let (_, _, ok) = fold_left check (empty, graph, true) res in
  ok
;;  

assert((make_test 100 10 30));;
assert((make_test 100 10 30));;
assert((make_test 100 10 30));;
assert((make_test 100 10 30));;
assert((make_test 1000000 1000 10000));;
assert((make_test 1000000 1000 100000));;
assert((make_test 1000000 1000 1000));;
assert((make_test 1000000 1000 10000));;
assert((make_test 1000000 1000 10000));;
 
let _ = print_string("\n42\n"); 
  
